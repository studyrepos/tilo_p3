% TiLo 2020 - P3 - Ex1 - List

% prefix
prefix(Xs, Ys) :-                           % Xs is head of Ys
    append(Xs, _, Ys).                      % something concatenated to list Xs results in list Ys

% postfix(Xs, Ys) :- Xs is tail of Ys

% Method 1: prefix/2 and reverse/2
postfix1(Xs, Ys) :-
    reverse(RXs, Xs),                       % reverse list Xs
    reverse(RYs, Ys),                       % reverse list Ys
    prefix(RXs, RYs).                       % reversed list Xs is prefix of reversed list Ys

% Method 2: append/3
postfix2(Xs, Ys) :-
    append(_, Xs, Ys).                      % list Xs concatenated to something results in list Ys

% postfix1([d, e], [a,b,c,d,e])
% postfix1([e], [a,b,c,d,e])
% postfix1([], [a,b,c,d,e])
% postfix1([a], [a,b,c,d,e])
% postfix1([c], [a,b,c,d,e])
% postfix1(Zs, [a,b,c,d,e])




% TiLo 2020 - P3 - Ex2+3 - BinaryTree

% base definition for a node
empty.                                      % empty node
node(_Val, _Ltree, _Rtree).                 % a node as a value with a left and right subtree

% base definition for a binary tree
bintree(empty).                             % an empty tree without nodes or subtrees
bintree(node(_, Ltree, Rtree)) :-           % a binary tree with a node containing a value and two subtrees
    bintree(Ltree), bintree(Rtree).         % the subtrees are binary trees either

% Ex2
% membertree(X, Ytree) :- X is member of tree Ytree
membertree(empty, empty).                   % an empty node is member of an empty tree
membertree(X, node(X, _, _)).               % X is value of root node
membertree(X, node(_, Ltree, _)) :-         % X may be member of left subtree
    membertree(X, Ltree),                   % recursive call an left subtree
    Ltree \== empty,                        % there could be no members in an empty tree
    bintree(Ltree).                         % left subtree has to be a valid binary tree
membertree(X, node(_, _, Rtree)) :-         % X may be member of right subtree
    membertree(X, Rtree),                   % recursive call an right subtree
    Rtree \== empty,                        % there could be no members in an empty tree
    bintree(Rtree).                         % right subtree has to be a valid binary tree

% membertree(Z, node(a, node(b, empty, empty), node(c, empty, empty)))



% Ex3
% Traverse tree in preorder, return list of values
preorder(empty,[]).                         % empty tree without notes returns an empty list
preorder(node(Value, Ltree, Rtree), [Value|Ys]):-   % call for not empty tree, root value of tree is start of list
    preorder(Ltree, Yls),                   % recursive call for left subtree, returning partial list for this subtree
    preorder(Rtree, Yrs),                   % recursive call for right subtree, returning partial list for this subtree
    append(Yls, Yrs, Ys).                   % concat left and right sublist and set result as tail of result list

% preorder(node(a, node(b, empty, empty), node(c, empty, empty)), Zs)


% Traverse tree in postorder, return list of values
postorder(empty, []).                       % empty tree without notes returns an empty list
postorder(node(Value, Ltree, Rtree), Ys) :- % call for not empty tree
    postorder(Ltree, Yls),                  % recursive call for left subtree, returning partial list for this subtree
    postorder(Rtree, Yrs),                  % recursive call for right subtree, returning partial list for this subtree
    append(Yls, Yrs, Ylrs),                 % concat sublists
    append(Ylrs, [Value], Ys).              % append root value to list of sublists, becoming last element of result list

% postorder(node(a, node(b, empty, empty), node(c, empty, empty)), Zs)


% Traverse list of trees, return list of root values
roots([],[]).                               % an empty list of trees has no root values
roots([empty|Xbs], Ys) :-                   % an empty tree should be ignored
    roots(Xbs, Ys).                         % recursive call for remaining trees in list until base case
roots([node(Value, _, _)|Xbs], Ys) :-       % call for not empty tree as head of list
    roots(Xbs, Y1s),                        % recursive call for tail of tree list, returning sublist
    append([Value], Y1s, Ys).               % concat root value of first tree with list of subroots to result list

% roots([node(a, empty, empty), node(bb, empty, empty), node(c, empty, empty)], Zs)
